要使用Docker Compose创建一个新的Kafka主题，您需要连接到运行Kafka容器的Shell中，并使用Kafka的命令行工具创建主题。以下是一些步骤，假设您已经在Kafka容器上运行Docker Compose：

1. 进入Kafka容器的Shell。您可以使用以下命令：

   ```bash
   docker exec -it kafka /bin/bash
   ```

   这将使您进入Kafka容器的命令行环境。

2. 使用Kafka的`kafka-topics.sh`命令创建新的主题。例如，要创建一个名为`my_topic`的主题，可以运行：

   ```bash
   kafka-topics.sh --create --topic shiguiwu.topic --partitions 1 --replication-factor 1 --bootstrap-server 192.168.59.140:9092
   ```

   这将创建一个名为`my_topic`的主题，具有1个分区和1个复制因子，并使用Kafka服务器的默认地址`localhost:9092`。

3. 您可以使用以下命令列出所有已创建的主题，以确保您的主题已成功创建：

   ```bash
   kafka-topics.sh --list --bootstrap-server 192.168.59.140:9092
   ```

   这将显示已创建的主题列表，您应该能够在列表中看到您刚刚创建的`my_topic`。

4. 退出Kafka容器的Shell：

   ```bash
   exit
   ```

现在，您已经成功创建了一个新的Kafka主题。请注意，这只是一个示例，您可以根据需要调整主题的分区数、复制因子和其他配置选项。确保在创建主题时使用适当的配置。