贡献代码步骤

1，fork源码到自己仓库

2，git clone 到自己本地

3，git config --global -l ，查看本地git账户的信息，要和github一致

4，除了自己的远程仓库外，还需要另外增加远程仓库

```shell
git remote add upstream  https://github.com/opengoofy/hippo4j.git
```

5，获取远程仓库所有变更 
```shell
git fetch upstream
```

6， 克隆分支

7，提交 pr

7.1 此时，你已经修改了很多代码，并且本地也进行了很多测试，然后就可以提交更改，最后提交 pr 了。

 获取远程项目的所有变更
 ```
 git fetch upstream
 ```
  
7.2 rebase 远程项目更改
这一步，我同样以 idea 界面操作来演示。

![](https://img-blog.csdnimg.cn/f2481a472f5545ceb47ce25d0aa6d446.png)

比如，现在我们想将自己本地已经修改的分支代码提交到远程仓库的 dev 分支，那就需要将远程仓库的 dev 分支代码 rebase 到自己的本地分支，下面是 idea 中的操作。


7.3 提交修改到自己的仓库
操作成功之后，就可以将自己修改的代码提交到自己的仓库，最后再提交 PR 了
![](https://img-blog.csdnimg.cn/4c013bca86fb4535a6092e6a7bc6e8f4.png)
同时也可以根据修改的内容，将修改过的文件放到不同的提交点里面，也就是每次提交，只提交当前提交点涉及到的文件。之后项目管理员进行代码 review 时，会更清晰。
进入 git 窗口，然后切换到自己的本地分支，就可以看到自己刚才提交的多个提交点了，之后就可以将这个分支所有更改提交到自己的仓库了。
一定要注意，这一步是将更改提交到自己的仓库，最好是进行强制推送，避免有些提交点没有提交到自己仓库对应的分支下。
7.5 提交 PR
打开远程仓库，然后点击“Pull requests”，就会出现下面的界面
![](https://img-blog.csdnimg.cn/4f828fbfc0f545e7a0d1943b17d73f99.png)
然后点击“Compare & pull request”，或者是“New pull request”进入提交 PR 的界面
![xx](https://img-blog.csdnimg.cn/2fa42615a4f44db5b78014bea918680c.png)
填写好一些必要的信息之后，就可以进行 PR 的提交了。

5.5. 其他
每次提交 PR 之前，都需要通过 git fetch upstream 命令来获取远程仓库的所有更改，然后将远程仓库的 dev 分支 rebase 到自己的本地分支，然后提交更改，否则最后提交 PR 时，可能会显示代码有冲突