version: "3.5"
services:
  ldap:
    image: osixia/openldap
    container_name: ldap
    hostname: ldap
    privileged: true
    user: root
    ports:
      - 389:389
      - 636:636
    environment:
      - LDAP_ORGANISATION=devops
      - LDAP_DOMAIN=shiguiwu.com
      - LDAP_ADMIN_PASSWORD=123456 
      - SET_CONTAINER_TIMEZONE=true
      - CONTAINER_TIMEZONE=Asia/Shanghai
    volumes:
      - /etc/localtime:/etc/localtime:ro
      - /opt/openldap/ldap:/var/lib/ldap:Z
      - /opt/openldap/slapd.d:/etc/ldap/slapd.d:Z
    restart: always
    tty: true #默认的用户名是admin 192.168.61.210
	
	