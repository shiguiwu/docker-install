version: '3'
services:
  ftp:
    image: stilliard/pure-ftpd:hardened
	container_name: ftp
    ports:
      - "20-21:20-21"
      - "30000-30009:30000-30009"
    volumes:
      - ./data:/home/ftpusers
    environment:
      - FTP_USER=user
      - FTP_PASS=pa44w0rd@aaAA11--
      - FTP_PASSIVE_PORTS=30000:30009
      - FTP_UID=1000
      - FTP_GID=1000
	  
	  
#上面作废	  
------------------------------------------------------------------------------

version: "3"
services:
  vsftpd:
    image: fauria/vsftpd
    container_name: vsftpd
    restart: always
    ports:
      - "20:20"
      - "21:21"
      - "20000:20000"
    volumes:
      - ./data:/home/vsftpd
      - ./log/vsftpd:/var/log/vsftpd
    environment:
      - FTP_USER=admin
      - FTP_PASS=admin1234
      - PASV_MIN_PORT=20000
      - PASV_MAX_PORT=20000
      - PASV_ADDRESS=xxx.xxx.xxx.xxx
      - LOG_STDOUT=1
	  
	  
	  
 # 注：这里指定的FTP_USER为admin，以及密码，还有对应的PASV_ADDRESS需改为自己的宿主机ip。 
 # PASV_MIN_PORT和PASV_MAX_PORT映射的是被动模式下端口使用范围必须在服务器的防火墙中开启20、21、22和21100/21110端口
