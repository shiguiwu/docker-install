#!/bin/sh
RESOURCE_NAME=drain-v0.0.8.jar
PID=$(ps -ef | grep $RESOURCE_NAME | grep -v grep | awk '{ print $2 }')
if [ -z "$PID" ]
then
echo Application is already stopped
else
echo kill -9 $PID
kill -9 $PID
fi
