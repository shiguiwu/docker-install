
#!/bin/sh
RESOURCE_NAME=drain-v0.0.8.jar
tpid=`ps -ef|grep $RESOURCE_NAME|grep -v grep|grep -v kill|awk '{print $2}'`
if [ ${tpid} ]; then
echo 'Stop Process...'
kill -15 $tpid
fi
sleep 5
tpid=`ps -ef|grep $RESOURCE_NAME|grep -v grep|grep -v kill|awk '{print $2}'`
if [ ${tpid} ]; then
echo 'Kill Process!'
kill -9 $tpid
else
echo 'Stop Success!'
fi
 
tpid=`ps -ef|grep $RESOURCE_NAME|grep -v grep|grep -v kill|awk '{print $2}'`
if [ ${tpid} ]; then
    echo 'drain-v0.0.8.jar is running.'
else
    echo 'drain-v0.0.8.jar is NOT running.'
fi
 
rm -f tpid
nohup java -jar -Dfile.encoding=utf-8 -Duser.timezone=Asia/Shanghai ./$RESOURCE_NAME  --spring.profiles.active=test >/dev/null 2>&1 &
echo $! > tpid
echo $RESOURCE_NAME Start Success!
