#!/bin/sh

#####################################################################
##                         功能说明                                ##
##                                                                 ##
##                        停止服务器                               ##
##                                                                 ##
#####################################################################

#设定系统运行环境变量
LC_ALL="zh_CN.UTF-8"
LANG="zh_CN.UTF-8"
export LC_ALL
export LANG

SERVER_BIN_DIR=$(cd "$(dirname "$0")"; pwd)

SERVER_DIR=$(cd "$SERVER_BIN_DIR";cd "..";pwd)

#echo 脚本目录:$SERVER_BIN_DIR

#读取配置文件
SERVER_NAME=`sed '/^SERVER_NAME=/!d;s/.*=//' $SERVER_BIN_DIR/deploy.conf`


#设定服务程序路径与名称(注意:此路径是文件路径,包含执行文件名称)
SERVER_PATH="$SERVER_DIR/**.jar"



#查看进程是否存在
RESULT=$(ps -ef | grep ${SERVER_PATH} | grep -v "grep")


#判断RESULT是否不为空，不为空则说明进程已经启动
if [ -n "$RESULT" ]; then
    echo $SERVER_NAME 运行中。
	PID=$(ps -ef | grep ${SERVER_PATH} | grep -v "grep" | awk '{printf $2}')
	
	if ((${PID} > 0)); then
        kill  ${PID}
    fi
    echo $SERVER_NAME 已关闭。
else
    echo $SERVER_NAME 未运行。
fi

