#!/bin/sh

#####################################################################
##                         功能说明                                ##
##                                                                 ##
##      若服务器已经启动，则先停止服务器。再启动服务器             ##
##                                                                 ##
#####################################################################


SERVER_BIN_DIR=$(cd "$(dirname "$0")"; pwd)

SERVER_DIR=$(cd "$SERVER_BIN_DIR";cd "..";pwd)

echo 脚本目录：$SERVER_BIN_DIR
echo 项目目录：$SERVER_DIR

#读取配置文件
SERVER_NAME=`sed '/^SERVER_NAME=/!d;s/.*=//' $SERVER_BIN_DIR/deploy.conf`
VM=`sed '/^VM=/!d;s/.*=//' $SERVER_BIN_DIR/deploy.conf`


#设定系统运行环境变量
LC_ALL="zh_CN.UTF-8"
LANG="zh_CN.UTF-8"
export LC_ALL
export LANG


#设定服务程序路径与名称(注意:此路径是文件路径,包含执行文件名称)
SERVER_PATH="$SERVER_DIR/**.jar"


#查看进程是否存在
RESULT=$(ps -ef | grep ${SERVER_PATH} | grep -v "grep")


#判断RESULT是否不为空，不为空则说明进程已经启动
if [ -n "$RESULT" ]; then
    echo [$SERVER_NAME] 正在运行
else
	#启动服务器
	#建议补充jvm优化参数，将jar启动为服务
	source /etc/profile
    nohup java -jar $VM $SERVER_PATH >/dev/null 2>&1 &

	echo  [$SERVER_NAME] 启动完成
fi



